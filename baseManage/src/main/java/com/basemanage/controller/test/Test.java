package com.basemanage.controller.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.basemanage.core.model.Condition;
import com.basemanage.core.model.Operators;
import com.basemanage.core.view.InvokeResult;
import com.basemanage.model.base.BaseTest;
import com.jfinal.kit.StrKit;

@SuppressWarnings("serial")
public class Test extends BaseTest<Test> {
	public static final Test dao = new Test();

	/**
	 * 保存/更新
	 * @param wxUser
	 * @return
	 */
	public InvokeResult saveTest(Test test) {
		if (test.getInt("id") != null) {
			test.setUpdateTime(new Date());
			test.update();
		} else {
			test.setCreateTime(new Date());
			test.save();
		}
		return InvokeResult.success();
	}

	/**
	 * 删除
	 * @param ids
	 * @return
	 */
	public InvokeResult delTest(String ids) {
		List<Integer> idsList = new ArrayList<Integer>();

		for (String idstr : ids.split(",")) {

			if (StrKit.isBlank(idstr))
				continue;

			idsList.add(Integer.valueOf(idstr));
		}

		Set<Condition> conditions = new HashSet<Condition>();
		conditions.add(new Condition("id", Operators.IN, idsList));
		dao.delete(conditions);

		return InvokeResult.success();
	}

}