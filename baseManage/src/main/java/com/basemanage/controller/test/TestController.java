package com.basemanage.controller.test;

import java.util.HashSet;
import java.util.Set;

import com.basemanage.core.controller.JCBaseController;
import com.basemanage.core.model.Condition;
import com.basemanage.core.model.Operators;
import com.basemanage.core.util.CommonUtils;
import com.basemanage.core.util.JqGridModelUtils;
import com.basemanage.core.view.InvokeResult;
import com.basemanage.model.Test;
import com.jfinal.plugin.activerecord.Page;

public class TestController extends JCBaseController {
	
	public void index() {
		render("test_index.html");
	}
	
	/**
	 * 跳转（添加/编辑）页面
	 */
	public void addTest(){
		Integer id=getParaToInt("id");
		String action="add";
		if(id!=null){
			this.setAttr("item", Test.dao.findById(id));
			action="edit";
		}
		this.setAttr("action", action);
		render("test_add.html");
	}
	
	
	/**
	 * 数据分页列表
	 */
	public void getListData() {
		
		String name=this.getPara("name");
		Set<Condition> conditions=new HashSet<Condition>();
		if(CommonUtils.isNotEmpty(name)){
			conditions.add(new Condition("remark",Operators.LIKE,name));
		}
		
		Page<Test> pageInfo=Test.dao.getPage(this.getPage(), this.getRows(),conditions,this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo)); 
	}
	
	/**
	 * 保存数据
	 */
	public void saveTest(){
		Test test=getModel(Test.class,"obj");
		InvokeResult result=Test.dao.saveTest(test);
		this.renderJson(result);
	}
	 
	/**
	 * 删除数据
	 */
	public void delTest(){
		String ids=this.getPara("ids");
		InvokeResult result=Test.dao.delTest(ids);
		this.renderJson(result);
	}
	
}