package com.basemanage.controller.product;

import com.alibaba.druid.sql.visitor.functions.Nil;
import com.basemanage.core.controller.JCBaseController;
import com.basemanage.core.util.JqGridModelUtils;
import com.basemanage.model.AppVersion;
import com.basemanage.model.XGoodsCategory;
import com.basemanage.service.AppVersionService;
import com.jfinal.plugin.activerecord.Page;

/*
 * 产品信息
 * 
 */
public class ProductController extends JCBaseController {
	
	/*
	 * 首页
	 */
	public void index(){
		render("index.html");
	}
	
	/*
	 * 获取分类的信息
	 */
	public void getCategorylist(){
		Page<XGoodsCategory> pageInfo=XGoodsCategory.dao.getPage(this.getPage(), this.getRows(),this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo)); 
	}
	/*
	 * 添加类别
	 */
	public void addCategory(){
		Integer id = getParaToInt("id");
		String action = "add";
		if (id!=null) {
			//编辑
			this.setAttr("item", XGoodsCategory.dao.findById(id));
			action = "edit";
		}
		this.setAttr("action", action);
		render("category_add.html");
	}
	/*
	 * 保存数据
	 */
	public void saveCategory(){
		
		
	}
	
	/*
	 * 产品列表
	 */
	 public void productlist(){
		 
	 }

}
