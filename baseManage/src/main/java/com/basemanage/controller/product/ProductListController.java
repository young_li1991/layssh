package com.basemanage.controller.product;

import com.basemanage.core.controller.JCBaseController;
import com.basemanage.core.util.JqGridModelUtils;
import com.basemanage.core.view.InvokeResult;
import com.basemanage.model.Test;
import com.basemanage.model.XGoods;
import com.basemanage.model.XGoodsCategory;
import com.jfinal.plugin.activerecord.Page;
/*
 * 产品列表
 */
public class ProductListController extends JCBaseController {
	
	/*
	 * 首页
	 */
	public void index(){
		render("index.html");
	}
	
	/*
	 * 产品列表
	 */
	public void getListData(){
		Page<XGoods> pageinfo = XGoods.dao.getPage(this.getPage(), this.getRows(), this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageinfo));
	}
	/*
	 * 添加产品视图
	 */
	public void addproduct(){
		
		Integer id = getParaToInt("id");
		
		String action = "add";
		
		
		if (id != null) {
			this.setAttr("item", XGoods.dao.findById(id));
			action = "edit";
		}
		this.setAttr("categoryList", XGoodsCategory.dao.find("select * from x_goods_category ORDER BY id asc"));
		this.setAttr("action", action);
		render("list_add.html");
		
	}
	/**
	 * 保存数据
	 */
	public void saveProduct(){
		XGoods goods=getModel(XGoods.class,"obj");
		InvokeResult result=XGoods.dao.saveGoods(goods);
		this.renderJson(result);
	}


}
