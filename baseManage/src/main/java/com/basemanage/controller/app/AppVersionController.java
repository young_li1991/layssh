package com.basemanage.controller.app;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.basemanage.core.controller.JCBaseController;
import com.basemanage.core.util.DateUtils;
import com.basemanage.core.util.FileUtils;
import com.basemanage.core.util.JqGridModelUtils;
import com.basemanage.core.view.InvokeResult;
import com.basemanage.model.AppVersion;
import com.basemanage.service.AppVersionService;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;

/**
 * APP管理
 */
public class AppVersionController extends JCBaseController {
	
	/**
	 * 跳转列表页面
	 */
	public void index() {
		render("index.html");
	}
	
	/**
	 * 获取数据列表
	 */
	public void getListData() {
		Page<AppVersion> pageInfo=AppVersionService.dao.getPage(this.getPage(), this.getRows(),this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo)); 
	}
	
	/**
	 * 启用/禁用
	 */
	public void setVisible(){
		int visible=this.getParaToInt("visible");
		String ids=this.getPara("ids");
		InvokeResult result=AppVersionService.me.setVisible(ids, visible);
		this.renderJson(result);
	}
	
	/**
	 * 跳转添加页面
	 */
	public void addApp(){
		Integer id=getParaToInt("id");
		String action="add";
		if(id!=null){
			this.setAttr("item", AppVersionService.dao.findById(id));
			action="edit";
		}
		this.setAttr("action", action);
		render("app_add.html");
	}
	
	/**
	 * 保存App信息
	 */
	public void saveApp(){
		Integer id=getParaToInt("id");
		String content=getPara("content");
		String linkUrl=getPara("linkUrl");
		Integer natureNo=getParaToInt("natureNo");
		String os=getPara("os");
		String url=getPara("url");
		String versionNo=getPara("versionNo");
		Integer status=getParaToInt("status");
		Integer isForce=getParaToInt("isForce");
		InvokeResult result=AppVersionService.me.saveAppVersion(id, content, linkUrl,
				natureNo,os, url, versionNo, status, isForce);
		this.renderJson(result);
	}
	
	/**
	 * 上传apk包
	 */
	public void uploadApk() {
		String dataStr=DateUtils.format(new Date(), "yyyyMMddHHmm");
		List<UploadFile> flist = this.getFiles("/temp", 1024*1024*50);
		Map<String,Object> data=new HashMap<String, Object>();
		if(flist.size()>0){
			UploadFile uf=flist.get(0);
			String fileUrl="apk/"+dataStr+"/"+uf.getFileName();
			String newFile=PropKit.get("uploadPath")+fileUrl;
			FileUtils.mkdir(newFile, false); 
			FileUtils.copy(uf.getFile(), new File(newFile), BUFFER_SIZE);
			uf.getFile().delete();
			data.put("fileUrl",fileUrl);
			renderJson(data);
		}
	}
}