package com.basemanage.controller.app;

import com.alibaba.druid.sql.ast.expr.SQLSequenceExpr.Function;
import com.basemanage.core.controller.JCBaseController;
import com.basemanage.core.util.JqGridModelUtils;
import com.basemanage.model.AppVersion;
import com.basemanage.service.AppVersionService;
import com.jfinal.plugin.activerecord.Page;

public class WxappController extends JCBaseController {
	
	
	
	/*
	 * 首页
	 */
	public void index(){
		render("index.html");
	}

	/**
	 * 获取数据列表
	 */
	public void getListData() {
		Page<AppVersion> pageInfo=AppVersionService.dao.getPage(this.getPage(), this.getRows(),this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo)); 
	}
	
}
