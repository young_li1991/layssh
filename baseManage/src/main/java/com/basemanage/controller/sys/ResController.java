package com.basemanage.controller.sys;


import java.util.HashMap;
import java.util.Map;

import com.basemanage.core.controller.JCBaseController;
import com.basemanage.core.view.InvokeResult;
import com.basemanage.model.SysRes;
import com.jfinal.kit.JsonKit;

public class ResController extends JCBaseController {
	
	/**
	 * 跳转列表
	 */
	public void index() { 
		this.render("res_index.html");
		
	}
	
	/**
	 * 获取树形表格数据
	 */
	public void  getTreeGridView(){
		this.renderJson(SysRes.dao.getTreeGridView());
	}
	
	/**
	 * 跳转添加页面
	 */
	public void  add(){
		Integer id=getParaToInt("id");
		if(id!=null){
			SysRes sysRes=SysRes.dao.getById(id);
			if(sysRes!=null){
				Integer pid = (Integer)sysRes.getInt("pid");
				if(pid!=null){//获取父资源
					SysRes pRes=SysRes.dao.getById(pid);
					setAttr("pRes",pRes);
				}
			}
		setAttr("sysRes",sysRes);
		}
		setAttr("jsonTree",JsonKit.toJson(SysRes.dao.getZtreeViewList()));
		this.render("res_add.html");
	}
	

	/**
	 * 保存
	 */
	public void saveRes(){
		Integer id=getParaToInt("id");
		String name=getPara("name");
		String seq=getPara("seq");
		String url=getPara("url");
		String iconCls=getPara("iconCls");
		Integer type=getParaToInt("type");
		Integer pId=getParaToInt("pid");
		int result=SysRes.dao.saveRes(id, name, seq, url, iconCls, type, pId);
		Map<String,Object> mapData=new HashMap<String,Object>();
		mapData.put("code", "success");
		if(result==1){
			mapData.put("result", "添加成功！！");
		}else if(result==2){
			mapData.put("result", "编辑成功！！");
		}
		this.renderJson(mapData);
	}

	/**
	 * 启用/禁用
	 */
	public void setEnabled(){
		Integer resId=getParaToInt("resId");
		Integer status=getParaToInt("status");
		SysRes.dao.setEnabled(resId, status);
		this.renderJson(InvokeResult.success());
	}
	
}





