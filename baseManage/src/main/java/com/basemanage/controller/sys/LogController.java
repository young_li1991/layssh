 package com.basemanage.controller.sys;

import java.util.HashSet;
import java.util.Set;

import com.basemanage.core.auth.interceptor.SysLogInterceptor;
import com.basemanage.core.controller.JCBaseController;
import com.basemanage.core.model.Condition;
import com.basemanage.core.model.Operators;
import com.basemanage.core.util.CommonUtils;
import com.basemanage.core.util.JqGridModelUtils;
import com.basemanage.model.SysLog;
import com.jfinal.aop.Clear;
import com.jfinal.plugin.activerecord.Page;
/**
 * 日志管理.
 */
@Clear(SysLogInterceptor.class)
public class LogController extends JCBaseController {

	/**
	 * 跳转日志列表
	 */
	public void index() {
		render("log_index.html");
	}
	
	/**
	 * 获取日志列表
	 */
	public void getListData() {
		String title=this.getPara("title");
		Set<Condition> conditions=new HashSet<Condition>();
		if(CommonUtils.isNotEmpty(title)){
			conditions.add(new Condition("class_name",Operators.LIKE,title));
		}
		Page<SysLog> pageInfo=SysLog.dao.getPage(getPage(), this.getRows(),conditions,this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo)); 
	}
}





