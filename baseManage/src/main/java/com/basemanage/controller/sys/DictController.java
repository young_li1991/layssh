package com.basemanage.controller.sys;

import java.util.HashSet;
import java.util.Set;

import com.basemanage.core.controller.JCBaseController;
import com.basemanage.core.model.Condition;
import com.basemanage.core.model.Operators;
import com.basemanage.core.util.CommonUtils;
import com.basemanage.core.util.JqGridModelUtils;
import com.basemanage.core.view.InvokeResult;
import com.basemanage.model.DictData;
import com.basemanage.model.DictType;
import com.jfinal.plugin.activerecord.Page;

/**
 * 字典控制器
 */
public class DictController extends JCBaseController {
	
	/**
	 * 跳转列表
	 */
	public void index() {
		render("type_index.html");
	}
	
	/**
	 * 添加类别
	 */
	public void add_type() {
		Integer id=this.getParaToInt("id");
		if(id!=null){
			this.setAttr("item", DictType.dao.findById(id));
		}
		render("type_add.html");
	}

	/**
	 * 添加数据
	 */
	public void add_data() {
		Integer id=this.getParaToInt("id");
		if(id!=null){
			this.setAttr("item", DictData.dao.findById(id));
		}
		this.setAttr("typeId", this.getParaToInt("typeId"));
		render("data_add.html");
	}

	/**
	 * 跳转数据编辑页面
	 */
	public void data_index() {
		Integer typeId=this.getParaToInt("typeId");
		this.setAttr("typeId", typeId);
		render("data_index.html");
	}
	
	/**
	 * 保存类型
	 */
	public void saveType(){
		Integer id=this.getParaToInt("id");
		String name=this.getPara("name");
		String remark=this.getPara("remark"); 
		InvokeResult invokeResult=DictType.dao.saveDictType(id,name,remark);
		this.renderJson(invokeResult);
	}
	
	/**
	 * 保存数据
	 */
	public void saveData(){
		Integer id=this.getParaToInt("id");
		Integer seq=this.getParaToInt("seq");
		String value=this.getPara("value");
		Integer typeId=this.getParaToInt("typeId");
		String name=this.getPara("name");
		String remark=this.getPara("remark"); 
		InvokeResult invokeResult=DictData.dao.saveDictData(id,name,remark,seq,value,typeId);
		this.renderJson(invokeResult);
	}
	
	/**
	 * 删除数据
	 */
	public void deleteData(){
		Integer id=this.getParaToInt("id");
		InvokeResult invokeResult=DictData.dao.deleteData(id);
		this.renderJson(invokeResult);
	}
	
	/**
	 *获得指定类型的数据列表
	 */
	public void getTypeListData() {
		String keyword=this.getPara("keyword");
		Set<Condition> conditions=new HashSet<Condition>();
		if(CommonUtils.isNotEmpty(keyword)){
			conditions.add(new Condition("name",Operators.LIKE,keyword));
		}
		Page<DictType> pageInfo=DictType.dao.getPage(getPage(), this.getRows(),conditions,this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo)); 
	}
	
	/**
	 * 获取类型列表
	 */
	public void getListData() {
		Integer typeId=this.getParaToInt("typeId");
		String keyword=this.getPara("keyword");
		Set<Condition> conditions=new HashSet<Condition>();
		if(CommonUtils.isNotEmpty(keyword)){
			conditions.add(new Condition("name",Operators.LIKE,keyword));
		}
		conditions.add(new Condition("dict_type_id",Operators.EQ,typeId));
		Page<DictData> pageInfo=DictData.dao.getPage(getPage(), this.getRows(),conditions,this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo)); 
	}
}





