package com.basemanage.controller.sys;

import com.basemanage.core.controller.JCBaseController;
import com.basemanage.core.util.JqGridModelUtils;
import com.basemanage.core.view.InvokeResult;
import com.basemanage.model.SysRole;
import com.basemanage.model.SysUser;
import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.tx.Tx;

/**
 * 系统用户管理.
 */
public class UserController extends JCBaseController {
	
	/**
	 * 跳转列表
	 */
	public void index() {
		render("user_index.html");
	}

	/**
	 * 获取列表数据
	 */
	public void getListData() {
		String keyword=this.getPara("name");
		Page<SysUser> pageInfo=SysUser.dao.getSysUserPage(getPage(), this.getRows(),keyword,this.getOrderbyStr());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo)); 
	}
	

	/**
	 * 启用/禁用
	 */
	public void setVisible(){
		Integer visible=this.getParaToInt("visible");
		String ids=this.getPara("ids");
		InvokeResult result=SysUser.dao.setVisible(ids, visible);
		this.renderJson(result);
	}

	/**
	 * 跳转添加页面
	 */
	public void add() {
		Integer id=this.getParaToInt("id");
		if(id!=null){
			this.setAttr("item", SysUser.dao.findById(id));
		}
		this.setAttr("id", id);
		render("user_add.html");
	}
	
	/**
	 * 保存
	 */
	public void save(){
		String username=this.getPara("name");
		String password=this.getPara("password");
		String phone=this.getPara("phone");
		String email=this.getPara("email");
		Integer id=this.getParaToInt("id");
		String des=this.getPara("des");
		InvokeResult result=SysUser.dao.save(id, username, password, des,phone, email);
		this.renderJson(result); 
	}
	
	/**
	 * 获取用户角色列表
	 */
	public void userRoleSetting() {
		Integer uid=this.getParaToInt("uid");
		this.setAttr("item", SysUser.dao.findById(uid));
		String result=SysRole.dao.getRoleZtreeViewList(uid);
		this.setAttr("jsonTree", InvokeResult.success(result));
		render("user_role_setting.html");
	}
	
	
	/**
	 *修改用户角色
	 */
	@Before(Tx.class)
	public void saveUserRoles(){
		Integer uid=this.getParaToInt("id");
		String roleIds=this.getPara("roleIds");
		InvokeResult result=SysUser.dao.changeUserRoles(uid, roleIds);
		this.renderJson(result);
	}
}





