package com.basemanage.controller;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import com.basemanage.core.auth.interceptor.SysLogInterceptor;
import com.basemanage.core.controller.JCBaseController;
import com.basemanage.core.shiro.kit.PasswordKit;
import com.basemanage.core.shiro.plugin.ShiroInterceptor;
import com.basemanage.core.view.InvokeResult;
import com.basemanage.model.SysUser;
import com.jfinal.aop.Clear;
import com.jfinal.kit.StrKit;

@Clear(SysLogInterceptor.class)
public class IndexController  extends JCBaseController  {

	/**
	 * 主页
	 */
	public void index() {

		if(!this.getSubject().isAuthenticated()){
			render("login.html");
		}else{
			setAttr("userMenu", this.getSysUserMenuView());
			render("index.html");
		}
	}

	/**
	 * 首页
	 */
	public void home() {
		if(!this.getSubject().isAuthenticated()){
			render("login.html");
		}else{
			setAttr("userMenu", this.getSysUserMenuView());
			render("home.html");
		}
	}

	/**
	 * 跳转到登录页
	 */
	public void login() {

//		if(!this.getSubject().isAuthenticated()){
//			
//			System.out.print(getRequest().getRemoteAddr());
//		}else{
//			System.out.print("ok");
//		}
		render("login.html");
	}

	/**
	 * 用户登录
	 */
	public void dologin() {
		String imageCode = this.getPara("imageCode");
		String username = this.getPara("username");
		String password = this.getPara("password");
		try {  
		Subject subject = this.getSubject();  
		
		if(!subject.isAuthenticated()){
			if (StrKit.isBlank(imageCode)) {
				this.renderJson(InvokeResult.failure("请输入验证码"));
				return;
			}
		    String imageCodeSession = (String) this.getSessionAttr("imageCode");
			if (!imageCodeSession.toLowerCase().equals(imageCode.trim().toLowerCase())) {
				this.renderJson(InvokeResult.failure("验证码输入有误"));
				return;
			}
			
			UsernamePasswordToken token = new UsernamePasswordToken(username, password);
			subject.login(token);
			if(subject.isAuthenticated()){
				this.renderJson(InvokeResult.success("登录成功"));
				return;
			}
		}else{
			this.renderJson(InvokeResult.success("登录成功"));
			return;
		}
		
		}catch(UnknownAccountException e ) {
			this.renderJson(InvokeResult.failure("账户不存在"));
			return;
		}
		catch(LockedAccountException e){
			this.renderJson(InvokeResult.failure("您的账号已经被锁定，请联系管理员"));
			return;
		}
		catch(DisabledAccountException e){
			this.renderJson(InvokeResult.failure("账户或密码不正确"));
			return;
		}
		catch(ExcessiveAttemptsException ea){
			this.renderJson(InvokeResult.failure("登录已超过5次，被锁一个小时或联系管理员解锁"));
			return;
		}
		catch(AuthenticationException ae){
			this.renderJson(InvokeResult.failure("账户或密码不正确"));
			return;
		}
		catch(Exception e){
			e.printStackTrace();
			this.renderJson(InvokeResult.failure("登录发生异常"));
		}
	}
	
	/*
	 * 退出登录
	 */
	public void loginOut() {
		getSubject().logout();
		redirect("/login");
	}
	
	  /**
     * 用户未通过授权
     */
    public void unauthorized() {
        renderText("权限不足");
    }

	public void pwdSetting() {
		try {
			SysUser user = SysUser.dao.getByName(this.getUsername());
			this.setAttr("sysUser", user);
			render("sys/pwd_setting.html");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void savePwdUpdate() {
		String oldPwd =  PasswordKit.encryptPwd(getPara("oldPwd"));
		String newPwd = PasswordKit.encryptPwd(getPara("newPwd"));
		String pwdRepeat = PasswordKit.encryptPwd(getPara("pwdRepeat"));
		try {
			SysUser sysUser = SysUser.dao.getByName(this.getUsername());
			if (!sysUser.get("pwd").equals(oldPwd)) {
				this.renderJson(InvokeResult.failure(-3, "旧密码不正确"));
			} else {
				if (newPwd.equals(pwdRepeat)) {
					InvokeResult result = SysUser.dao.savePwdUpdate(sysUser.getInt("id"), newPwd);
					this.renderJson(result);
				} else {
					this.renderJson(InvokeResult.failure(-1, "两次密码不一致"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
