package com.basemanage.model;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

 
import com.basemanage.core.model.BaseModel;
import com.basemanage.core.model.Condition;
import com.basemanage.core.model.Operators;


@SuppressWarnings("serial")
public class AccessToken extends BaseModel<AccessToken>{

	public static AccessToken dao = new AccessToken();

	public void updateImToken(String token, String imToken) {
		// TODO Auto-generated method stub 
		Set<Condition> conditions= new HashSet<Condition>();
		conditions.add(new Condition("token",Operators.EQ,token));
		Map<String,Object> newValues=new HashMap<String,Object>();
		newValues.put("im_token", imToken);
		this.update(conditions, newValues);
	} 

	public AccessToken getAccessToken(String token) {
		Set<Condition> conditions=new HashSet<Condition>();
		conditions.add(new Condition("token",Operators.EQ,token));
		 return this.get(conditions);
	}
	public AccessToken createOrUpdateAccessToken(Integer uid) {
		Set<Condition> conditions=new HashSet<Condition>();
		conditions.add(new Condition("uid",Operators.EQ,uid));
		AccessToken accessToken=this.get(conditions);
		if(accessToken!=null){
			AccessToken.dao.delete(conditions);
		}
		accessToken=new AccessToken();
		accessToken.set("uid", uid);
		//accessToken.set("token",UUIDUtils.getUUIDKey());
		accessToken.set("modified_time", new Date());
		accessToken.set("create_time", new Date());
		accessToken.save();
		return accessToken;
	}

}