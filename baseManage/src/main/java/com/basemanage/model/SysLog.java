package com.basemanage.model;

import java.util.Date;

import com.basemanage.model.base.BaseSysLog;

/**
 * @author  
 * 系统日志
 */
@SuppressWarnings("serial")
public class SysLog extends BaseSysLog<SysLog>
{
	public static SysLog dao = new SysLog();
	
	public void saveSysLog(Integer uid,String ip,String from,String url,String className,String methodName,long startTime,int errCode,String errMsg){
		SysLog sysLog=new SysLog();
//		sysLog.setUid(uid);
		sysLog.setIp(ip);
		sysLog.setFrom(from);
		sysLog.setUrl(url);
		sysLog.setClassName(className);
		sysLog.setMethodName(methodName);
		sysLog.setErrCode(errCode);
		sysLog.setErrMsg(errMsg);
		sysLog.setStartTime(new Date(startTime));
		sysLog.setSpendTime(System.currentTimeMillis()-startTime);
		sysLog.save();
	}
}
