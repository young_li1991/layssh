package com.basemanage.model;

import javax.sql.DataSource;

import com.basemanage.conf.MainConfig;
import com.basemanage.core.plugin.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;


/**
 * 在数据库表有任何变动时，运行一下 main 方法，极速响应变化进行代码重构
 */
public class _CodeGenerator {
	
	public static DataSource getDataSource() {
		
		 DruidPlugin druidPlugin = MainConfig.getDruidPlugin();
	        druidPlugin.start();
	        return druidPlugin.getDataSource();
	        
		 
	}
	
	public static void main(String[] args) {
		// base model 所使用的包名
		String baseModelPackageName = "com.basemanage.model.base";
		// base model 文件保存路径
		System.getProperty("user.dir");
		//D:\dev_work\git\manage\baseManage
		String baseModelOutputDir = System.getProperty("user.dir")+"/src/main/java/com/baseManage/model/base/";
		// model 所使用的包名 (MappingKit 默认使用的包名)
		String modelPackageName = "com.basemanage.model";
		// model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
		String modelOutputDir = baseModelOutputDir + "/..";
		
		// 创建生成器
		Generator gernerator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
		// 添加不需要生成的表名
		gernerator.addExcludedTable("adv");
		//gernerator.addCludedTable("access_token");
		// 设置是否在 Model 中生成 dao 对象
		gernerator.setGenerateDaoInModel(true);
		// 设置是否生成字典文件
		gernerator.setGenerateDataDictionary(false);
		// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
		gernerator.setRemovedTableNamePrefixes("t_");
		// 生成
		gernerator.generate();
		
	}
}




