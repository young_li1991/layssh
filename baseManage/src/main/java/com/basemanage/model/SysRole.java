package com.basemanage.model;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.basemanage.core.cache.CacheClearUtils;
import com.basemanage.core.model.Condition;
import com.basemanage.core.model.Operators;
import com.basemanage.core.util.CommonUtils;
import com.basemanage.core.view.ZtreeView;
import com.basemanage.model.base.BaseSysRole;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Db;

/**
 * @author  
 * 系统角色
 */
public class SysRole extends BaseSysRole<SysRole>
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1925653378487209505L;
	/**
	 * 
	 */
	public static final SysRole dao = new SysRole(); 
	
	/**
	 * @param uid
	 * @return
	 */
	public List<SysRole> getSysRoleList(int uid){
		
		return this.find("select * from sys_role where id in(select role_id from sys_user_role where user_id =?)",uid);
	}
	
	public List<SysRole> getSysRoleIdList(int uid){
		return this.find("select id from sys_role where id in(select role_id from sys_user_role where user_id =?)",uid);
	}
	
	public int setVisible(String idStrs, Integer visible) {
		List<Integer> ids=CommonUtils.getIntegerListByStrs(idStrs);
		if(ids.contains(1)){
			return -1;// InvokeResult.failure("超级管理员不能被修改");
		}
		Set<Condition> conditions=new HashSet<Condition>();
		conditions.add(new Condition("id",Operators.IN,ids));
		Map<String,Object> newValues=new HashMap<String,Object>();
		newValues.put("status", visible);
		return 	this.update(conditions, newValues);
	} 

	public String getZtreeViewList(Integer type, Integer roleId) {
		//获取选中的菜单集合
		List<SysRes> curMenulist=this.getSysMenus(roleId);
		List<ZtreeView> ztreeViews=new ArrayList<ZtreeView>();
		ztreeViews.add(new ZtreeView(10000,null,"资源列表",true));
		for(SysRes SysRes : curMenulist){
			ZtreeView ztreeView=new ZtreeView();
			ztreeView.setId(SysRes.getInt("id"));
			ztreeView.setName(SysRes.getStr("name"));
			ztreeView.setOpen(true);
			if(SysRes.getInt("pid")==null){
				ztreeView.setpId(10000);
			}else{
				ztreeView.setpId(SysRes.getInt("pid"));
			}
			ztreeView.setChecked(SysRes.getLong("selected")==1?true:false);
			ztreeViews.add(ztreeView);
		}
		return  JsonKit.toJson(ztreeViews);
	}
	
	public String getRoleZtreeViewList(Integer uid) {
		List<SysRole> list=this.getSysRoles(uid);
		List<ZtreeView> ztreeViews=new ArrayList<ZtreeView>();
		ztreeViews.add(new ZtreeView(10000,null,"角色列表",true));
		for(SysRole role : list){
			ZtreeView ztreeView=new ZtreeView();
			ztreeView.setId(role.getId());
			ztreeView.setName(role.getName());
			ztreeView.setOpen(true);
			ztreeView.setpId(10000);
			ztreeView.setChecked(role.getLong("selected")==1?true:false);
			ztreeViews.add(ztreeView);
		}
		return JsonKit.toJson(ztreeViews);
	}
	
	public List<SysRes> getSysMenus(Integer roleId) {
		SysRole role=this.findById(roleId); 
		if(role!=null){
			List<SysRes> list=SysRes.dao.find("SELECT *,(CASE WHEN re.id IN (SELECT rr.res_id from sys_role_res rr WHERE rr.role_id="+roleId+" ) THEN 1 ELSE 0 END) as selected FROM sys_res re where re.enabled=1");
			return list;
		}
		return new ArrayList<SysRes>();
	}
	public List<SysRole> getSysRoles(Integer uid) {
		List<SysRole> list=this.find("SELECT *,(CASE WHEN re.id IN (SELECT rr.role_id from sys_user_role rr WHERE rr.user_id="+uid+" ) THEN 1 ELSE 0 END) as selected FROM sys_role re where re.status=1");
		return list;
	}
	
	public void saveMenuAssign(String menuIds, Integer roleId) {
		
		Db.update("delete from sys_role_res where role_id = ?", roleId);
		if(CommonUtils.isNotEmpty(menuIds)){//改成批量插入
			List<String> sqlList=new ArrayList<String>();
			for(String id : menuIds.split(",")){
				if(CommonUtils.isNotEmpty(id)){
					if(!Integer.valueOf(id).equals(10000))
						sqlList.add("insert into sys_role_res (role_id,res_id) values ("+roleId+","+Integer.valueOf(id)+")");
				}
			}
			Db.batch(sqlList, 50);
			CacheClearUtils.clearUserMenuCache();
		}
	}
	
	public void save(Integer id, String name, String des) {
		// TODO Auto-generated method stub
		if(id!=null){
			SysRole role=this.findById(id);
			role.set("name", name).set("des",des).update();
		}else{
			new SysRole().set("name", name).set("des",des).set("createdate", new Date()).save();
		}
	} 
	
//	public List<SysRole> getSysRoleNamelist(){
//		return this.find("select id,name from sys_role where status = 1");
//	}
	
	public String getRoleNames(String roleIds){
		String sql="select group_concat(name) as roleNames from sys_role where id in("+roleIds+")";
		SysRole sysRole=this.findFirst(sql);
		if(sysRole!=null){
			return sysRole.getStr("roleNames");
		}
		return "";
	}
}
