package com.basemanage.core.model;

import java.io.Serializable;
import java.util.List;

public class JqGridModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9005687551515231593L;
	/**
	 * 总记录数
	 */
	private long records;
	/**
	 * 第几页
	 */
	private int page;
	/**
	 * 总页数
	 */
	private int total;
	
	private List rows;

	public long getRecords() {
		return records;
	}

	public void setRecords(long records) {
		this.records = records;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List getRows() {
		return rows;
	}

	public void setRows(List rows) {
		this.rows = rows;
	}
	
}
