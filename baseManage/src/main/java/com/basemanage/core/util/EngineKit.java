package com.basemanage.core.util;

import java.util.Set;

import com.basemanage.core.taglib.DefineDirective;
import com.jfinal.kit.LogKit;
import com.jfinal.template.Directive;
import com.jfinal.template.Engine;
import com.xiaoleilu.hutool.util.ClassUtil;
import com.xiaoleilu.hutool.util.StrUtil;

/**
 * Created by jie on 2017/4/11.
 * 方便自定义标签、函数的统一维护
 */
public class EngineKit {
    private static final String defineDirective = "com.basemanage.core";

    /**
     * 配置第三方的Directive
     *
     * @param me Engine
     */
    private static void getThirdpartydDirective(Engine me) {
//        me.addDirective("assets", new AssetsDirective());
    }


    /**
     * 配置共享函数模板
     *
     * @param me Engine
     */
    public static void addSharedFunction(Engine me) {
    	// 使用JF模板渲染通用页面
    			me.addSharedFunction("/WEB-INF/view/common/basecss.html");
    			me.addSharedFunction("/WEB-INF/view/common/basejs.html");
    			me.addSharedFunction("/WEB-INF/view/common/top.html");
    			me.addSharedFunction("/WEB-INF/view/common/footer.html");
    }

    /**
     * 获取到包名下的所有自定义标签并加入到Engine 中
     *
     * @param me Engine
     */
    public static void addDefineDirective(Engine me) {
        Set<Class<?>> classes = ClassUtil.scanPackageByAnnotation(defineDirective, DefineDirective.class);
        for (Class<?> clazz : classes) {
            DefineDirective defineDirective = clazz.getAnnotation(DefineDirective.class);
            String tag = defineDirective.tag();
            if (StrUtil.isNotEmpty(tag)) {
                me.addDirective(tag, (Directive) ClassUtil.newInstance(clazz));
            } else {
                LogKit.error("自定义Directive的标签为空无效：" + clazz.getName());
            }
        }
        getThirdpartydDirective(me);
    }
}
