package com.basemanage.core.controller;
import java.util.LinkedHashMap;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import com.basemanage.core.util.CommonUtils;
import com.basemanage.model.SysRes;
import com.basemanage.model.SysUser;
import com.jfinal.core.Controller;

public abstract class JCBaseController extends Controller {
	public static final int BUFFER_SIZE = 1024 * 1024;
	
	 /**
     * 获取Subject 对象
     *
     * @return Subject
     */
    public Subject getSubject() {
        return SecurityUtils.getSubject();
    }
    
	  /**
     * 获取当前用户名
     *
     * @return username
     */
    public String getUsername() {
        Object principal = getSubject().getPrincipal();
        return principal.toString();
    }
    
    /**
     * 获取shiro session
     *
     * @return Session
     */
    public Session getShiroSession() {
        Subject subject = getSubject();
        return null != subject && null != subject.getSession() ? subject.getSession() : null;
    }
    
	/**
	 * 获取排序对象
	 * @author  	
	 * @return
	 */
	protected LinkedHashMap<String,String> getOrderby(){
		String sord=this.getPara("sord");
		String sidx=this.getPara("sidx");
		LinkedHashMap<String,String> orderby=new LinkedHashMap<String,String>();
		if(CommonUtils.isNotEmpty(sidx)){
			orderby.put(sidx, sord);
		}
		return orderby;
	}
	/**
	 * 获取排序字符串
	 * @author  	
	 * @return
	 */
	protected String getOrderbyStr(){
		String sord=this.getPara("sord");
		String sidx=this.getPara("sidx");
		if(CommonUtils.isNotEmpty(sidx)){
			return " order by "+ sidx+" "+sord;
		}
		return "";
	}
	/**
	 * 获取每几页
	 * @author  	
	 * @return
	 */
	protected int getPage(){
		return this.getParaToInt("page", 1);
	}
	/**
	 * 获取每页数量
	 * @author  	
	 * @return
	 */
	protected int getRows(){
		int rows=this.getParaToInt("rows", 20);
		if(rows>1000)rows=1000;
		return rows;
	}
	
	/**
	 * 获取用户菜单
	 */
	public String getSysUserMenuView(){
		SysUser user = SysUser.dao.getByName(this.getUsername());
		String menuView="";
		if(user!=null){
			int uid=user.getInt("id");
			 menuView=SysRes.dao.getSysUserMenuView(uid,"/");
		}  
		return menuView;
	}
}





