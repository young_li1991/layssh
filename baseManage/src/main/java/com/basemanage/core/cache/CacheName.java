package com.basemanage.core.cache;
/**
 * 缓存名称
 *
 */
public final class CacheName {
	/**
	 * 默认
	 */
	public static final String defaut_cache="andCache";
	/**
	 * 字典数据缓存
	 */
	public static final String dictCache="dictCache";
	/**
	 * 用户菜单缓存
	 */
	public static final String userMenuCache="userMenuCache";
}
