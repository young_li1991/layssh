package com.basemanage.core.cache;

import com.jfinal.plugin.ehcache.CacheKit;

public final class CacheClearUtils {
	/**
	 * 清除所有用户的资源缓存
	 */
	public static void clearUserMenuCache(){
		CacheKit.removeAll(CacheName.userMenuCache);
	}
	
	/**
	 * 清除字典缓存
	 */
	public static void clearDictCache(){
		CacheKit.removeAll(CacheName.dictCache);
	}
}
