package com.basemanage.core.shiro.tag;

import com.basemanage.core.taglib.DefineDirective;
import com.jfinal.kit.LogKit;
import com.jfinal.template.Env;
import com.jfinal.template.stat.Scope;

import java.io.IOException;
import com.jfinal.template.io.Writer;
import java.security.Principal;

/**
 * Created by jie on 2017/4/3.
 * 获取Subject Principal 信息
 * #principal()
 * body
 * #end
 */
@DefineDirective(tag = "principal")
public class PrincipalTag extends SecureTag {

    public void exec(Env env, Scope scope, Writer writer) {
        if (getSubject() != null && getSubject().getPrincipal() != null) {
            Object principal = getSubject().getPrincipal();
            try {
                writer.write(principal.toString());
            } catch (IOException e) {
                LogKit.error("PrincipalTag IOException");
                e.printStackTrace();
            }
        }
    }

}
