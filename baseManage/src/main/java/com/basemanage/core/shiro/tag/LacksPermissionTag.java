package com.basemanage.core.shiro.tag;

import com.basemanage.core.taglib.DefineDirective;
import com.jfinal.template.Env;
import com.jfinal.template.expr.ast.Expr;
import com.jfinal.template.expr.ast.ExprList;
import com.jfinal.template.stat.Scope;
import com.xiaoleilu.hutool.util.ArrayUtil;

import com.jfinal.template.io.Writer;

/**
 * Created by jie on 2017/4/3.
 * 验证当前Subject是否没有该权限
 * #lacksPermission(permissionName)
 * body
 * #end
 */
@DefineDirective(tag = "lacksPermission")
public class LacksPermissionTag extends SecureTag {
    private Expr[] exprs;

    public void setExprList(ExprList exprList) {
        exprs = exprList.getExprArray();
    }

    public void exec(Env env, Scope scope, Writer writer) {
        if (getSubject() != null && ArrayUtil.isNotEmpty(exprs))
            if (!getSubject().isPermitted(exprs[0].toString()))
                stat.exec(env, scope, writer);
    }

    public boolean hasEnd() {
        return true;
    }
}
