package com.basemanage.core.shiro.kit;

import java.io.Serializable;

/**
 * Created by jie on 2017/4/10.
 * 用户session 和username
 */
public class UserSessionVo implements Serializable {
    private String username;
    private String sessionId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
