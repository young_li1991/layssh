//package com.basemanage.core.shiro.cache;
//
//import org.apache.shiro.cache.Cache;
//import org.apache.shiro.cache.CacheException;
//import org.apache.shiro.cache.CacheManager;
//
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.ConcurrentMap;
//
///**
// * Created by jie on 2017/4/4.
// * 自定义 RedisCacheManager 
// */
//public class RedisCacheManager implements CacheManager {
//    private final ConcurrentMap<String, Cache> caches = new ConcurrentHashMap<String, Cache>();
//    /*永不过期*/
//    private int expire = 0;
//
//    public int getExpire() {
//        return expire;
//    }
//
//    public void setExpire(int expire) {
//        this.expire = expire;
//    }
//
//    public <K, V> Cache<K, V> getCache(String name) throws CacheException {
//        Cache c = caches.get(name);
//        if (c == null) {
//            c = new RedisCache<K, V>(expire);
//            caches.put(name, c);
//        }
//        return c;
//    }
//}
