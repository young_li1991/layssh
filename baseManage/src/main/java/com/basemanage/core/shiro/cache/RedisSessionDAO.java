//package com.basemanage.core.shiro.cache;
//
//import com.basemanage.core.shiro.kit.ShiroSessionKit;
//import com.jfinal.kit.LogKit;
//import com.jfinal.plugin.redis.Redis;
//import com.xiaoleilu.hutool.util.CollectionUtil;
//
//
//import org.apache.shiro.session.Session;
//import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;
//
//import java.io.Serializable;
//import java.util.Collection;
//import java.util.List;
//import java.util.Set;
//
///**
// * Created by jie on 2017/4/4.
// * 自定义实现SessionDAO,保存在Redis
// */
//public class RedisSessionDAO extends AbstractSessionDAO {
//    private static final String SHIRO_KEY = "session_";
//
//
//    /*永不过期*/
//    private int expire = 0;
//
//    public int getExpire() {
//        return expire;
//    }
//
//    public void setExpire(int expire) {
//        this.expire = expire;
//    }
//
//    /**
//     * 如DefaultSessionManager在创建完session后会调用该方法；
//     * 如保存到关系数据库/文件系统/NoSQL数据库；即可以实现会话的持久化；
//     * 主要此处返回的ID.equals(session.getId())；
//     *
//     * @param session session
//     * @return 返回会话ID
//     */
//    protected Serializable doCreate(Session session) {
//        Serializable sessionId = this.generateSessionId(session);
//        this.assignSessionId(session, sessionId);
//        this.saveSession(session);
//        return sessionId;
//    }
//
//    /**
//     * 根据会话ID获取会话
//     *
//     * @param serializable 会话ID
//     * @return Session
//     */
//    protected Session doReadSession(Serializable serializable) {
//        if (null == serializable) {
//            LogKit.error("session id is null");
//            return null;
//        }
//        return Redis.use().get(SHIRO_KEY + serializable);
//    }
//
//    /**
//     * 更新会话
//     * 如更新会话最后访问时间/停止会话/设置超时时间/设置移除属性等会调用
//     * 更新username --》 sessionID 关系 （不需要会话管理可删除 updSessionRelation）
//     *
//     * @param session session
//     */
//    public void update(Session session) {
//        ShiroSessionKit.updSessionRelation(session);
//        saveSession(session);
//    }
//
//    /**
//     * 删除会话
//     * 当会话过期/会话停止（如用户退出时）会调用
//     *
//     * @param session session
//     */
//    public void delete(Session session) {
//        if (null == session || null == session.getId()) {
//            LogKit.error("session or session id is null");
//        } else {
//            Redis.use().del(SHIRO_KEY + session.getId());
//            LogKit.debug("delete success shiro key " + session.getId());
//        }
//    }
//
//    /**
//     * 获取当前所有活跃用户，如果用户量多此方法影响性能
//     *
//     * @return Collection
//     */
//    public Collection<Session> getActiveSessions() {
//        Set<String> keys = Redis.use().keys(SHIRO_KEY + "*");
//        if (CollectionUtil.isNotEmpty(keys)) {
//            List<Session> sessionList = Redis.use().mget(keys);
//            return sessionList;
//        }
//        return null;
//    }
//
//    /**
//     * 保存 session
//     *
//     * @param session Session
//     */
//    private void saveSession(Session session) {
//        if (session == null || session.getId() == null) {
//            LogKit.error("session or session id is null");
//            return;
//        }
//        String key = SHIRO_KEY + session.getId();
//        Redis.use().setex(key, this.expire, session);
//    }
//}
