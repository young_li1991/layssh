package com.basemanage.core.shiro.tag;

import com.basemanage.core.taglib.DefineDirective;
import com.jfinal.template.Env;
import com.jfinal.template.stat.Scope;

import com.jfinal.template.io.Writer;

/**
 * Created by jie on 2017/4/3.
 * 用户已经身份验证/记住我登录后显示相应的信息。
 * #shiroUser()
 * body
 * #end
 */
@DefineDirective(tag = "shiroUser")
public class ShiroUserTag extends SecureTag {

    public void exec(Env env, Scope scope, Writer writer) {
        if (getSubject() != null && getSubject().getPrincipal() != null)
            stat.exec(env, scope, writer);
    }

    public boolean hasEnd() {
        return true;
    }
}
