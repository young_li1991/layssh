 package com.basemanage.conf;

import com.basemanage.controller.api.ApiController;
import com.basemanage.core.shiro.plugin.ShiroInterceptor;
import com.jfinal.aop.Clear;
import com.jfinal.config.Routes;
/**
 * APP端Routes配置
 */
public class ApiRoutes extends Routes{

	@Override
	public void config() {
//		@Clear(ShiroInterceptor.class);
		//
		add("/api",ApiController.class);
		
	}

}
