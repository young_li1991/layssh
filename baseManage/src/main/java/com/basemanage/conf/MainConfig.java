package com.basemanage.conf;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.basemanage.core.auth.interceptor.SysLogInterceptor;
import com.basemanage.core.shiro.plugin.ShiroInterceptor;
import com.basemanage.core.shiro.plugin.ShiroPlugin;
import com.basemanage.core.util.EngineKit;
import com.basemanage.model._MappingKit;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.template.Engine;

/**
 * API引导式配置
 */
public class MainConfig extends JFinalConfig {
	private static Prop p = loadConfig();
	private WallFilter wallFilter;

	private static Prop loadConfig() {
		try {
			return PropKit.use("profile.dev.properties");
		} catch (Exception e) {
			return PropKit.use("profile.sit.properties");
		}
	}
	/**
	 * 配置全局常量
	 *
	 * @param me
	 * Constants
	 */
	public void configConstant(Constants me) {
		me.setEncoding("utf-8");
		me.setDevMode(p.getBoolean("devMode", false));
		me.setBaseUploadPath(p.get("uploadPath"));
		me.setError404View("/page/404.html");
		me.setError500View("/page/500.html");
	}

	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.add(new AdminRoutes());
		me.add(new FrontRoutes());
		me.add(new ApiRoutes());
	}

	/**
	 * 配置模板引擎
	 *
	 * @param me
	 *  Engine
	 */
	@Override
	public void configEngine(Engine me) {
		EngineKit.addDefineDirective(me);
		EngineKit.addSharedFunction(me);
		me.addSharedObject("mainTitle", "管理系统");
	}

	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {

	 
		ShiroPlugin shiroPlugin = new ShiroPlugin();
		shiroPlugin.setLoginUrl("/login");
		shiroPlugin.setUnauthorizedUrl("/unauthorized");
		me.add(shiroPlugin);

		DruidPlugin druidPlugin = getDruidPlugin();
		wallFilter = new WallFilter();
		wallFilter.setDbType("mysql");
		druidPlugin.addFilter(wallFilter);
		druidPlugin.addFilter(new StatFilter());
		me.add(druidPlugin);
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		arp.setShowSql(p.getBoolean("devMode", false));
		arp.setDevMode(p.getBoolean("devMode", false));
		// 所有配置在 MappingKit 中搞定
		_MappingKit.mapping(arp);
		me.add(arp);
		
		me.add(new EhCachePlugin());
	}

	/**
	 * 创建DruidPlugin 抽成公告方法，方便生成model时候调用
	 * @return DruidPlugin
	 */
	public static DruidPlugin getDruidPlugin() {
		return new DruidPlugin(p.get("jdbcUrl"), p.get("user"), p.get("password").trim());
	}

	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
//		me.add(new ShiroInterceptor());
//		me.addGlobalActionInterceptor(new SysLogInterceptor());
	}

	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		me.add(new ContextPathHandler("ctx"));
	}

	/**
	 * 启动后回调
	 */
	public void afterJFinalStart() {
		wallFilter.getConfig().setSelectUnionCheck(false);
	}
	
	
	
	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		
		JFinal.start("src/main/webapp", 8090, "/", 5);
	}

}
