package com.basemanage.conf;

import com.basemanage.controller.ImageController;
import com.basemanage.controller.IndexController;
import com.basemanage.controller.app.AppVersionController;
import com.basemanage.controller.app.WxappController;
import com.basemanage.controller.img.AdvertController;
import com.basemanage.controller.product.ProductController;
import com.basemanage.controller.product.ProductListController;
import com.basemanage.controller.sys.DictController;
import com.basemanage.controller.sys.LogController;
import com.basemanage.controller.sys.ResController;
import com.basemanage.controller.sys.RoleController;
import com.basemanage.controller.sys.UserController;
import com.jfinal.config.Routes;


/**
 * 后台管理Routes配置
 */
public class AdminRoutes extends Routes {

	@Override
	public void config() {
		setBaseViewPath("/WEB-INF/view");

		add("/", IndexController.class);
		add("/sys/log", LogController.class, "/sys");
		add("/sys/res", ResController.class, "/sys");
		add("/sys/user", UserController.class, "/sys");
		add("/sys/role", RoleController.class, "/sys");
		add("/dict", DictController.class, "/sys/dict");
		add("/app", AppVersionController.class, "/app");
		add("/login/image", ImageController.class, "/image");
		add("/wxapp",WxappController.class,"/wxapp");
		add("/img/Advert",AdvertController.class,"/img/Advert");
		add("/product",ProductController.class,"/product/category");
		add("/product/list",ProductListController.class,"/product/list");
		
	}

}
