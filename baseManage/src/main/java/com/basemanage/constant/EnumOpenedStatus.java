package com.basemanage.constant;

/**
 * 菜单开关状态枚举
 */
public enum EnumOpenedStatus {
    CLOSE_STATUS("0", "close"),
    OPEN_STATUS("1", "open");

    private String code;
    private String msg;

    EnumOpenedStatus(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    /**
     * 根据枚举的code返回枚举对象
     *
     * @param code 枚举code值
     * @return 枚举对象
     */
    public static EnumOpenedStatus getEnumByCode(String code) {
        if (code == null) return null;
        for (EnumOpenedStatus status : values()) {
            if (status.getCode().equals(code.trim()))
                return status;
        }
        return null;
    }
}
