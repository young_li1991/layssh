package com.basemanage.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.basemanage.core.model.Condition;
import com.basemanage.core.model.Operators;
import com.basemanage.core.view.InvokeResult;
import com.basemanage.model.AppVersion;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;

public class AppVersionService {

	public static final AppVersionService me = new AppVersionService();
	public static final AppVersion dao = new AppVersion();
	public static final class OS {
		public static final String android = "android";
		public static final String ios = "ios";
	}
	
	public void setVisible(List<Integer> idsList, int status) {
		// TODO Auto-generated method stub
		Set<Condition> conditions=new HashSet<Condition>();
		conditions.add(new Condition("id",Operators.IN,idsList));
		Map<String,Object> newValues=new HashMap<String,Object>();
		newValues.put("status", status);
		dao.update(conditions, newValues);
	}

	public InvokeResult setVisible(String ids, int status) {
		List<Integer> idsList=new ArrayList<>();
		for(String idstr : ids.split(",")){
			if(StrKit.isBlank(idstr))continue;
			idsList.add(Integer.valueOf(idstr));
		}
		this.setVisible(idsList,status==0?0:1);
		return InvokeResult.success();
	}

	public InvokeResult saveAppVersion(AppVersion appVersion) {
		if(appVersion.getInt("id")!=null){
			appVersion.update();
		}else{
			appVersion.save();
		}
		return InvokeResult.success();
	}
	public InvokeResult saveAppVersion(Integer id,String content,String linkUrl,Integer natureNo,String os,
			String url,String versionNo,Integer status,Integer isForce){
		if(id!=null){
			AppVersion appVersion=dao.findById(id);
			appVersion.set("content", content).set("link_url", linkUrl).set("nature_no", natureNo)
			.set("os", os).set("url", url).set("version_no", versionNo).set("status", status).set("is_force", isForce).update();
		}else{
			new AppVersion().set("content", content).set("link_url", linkUrl).set("nature_no", natureNo)
			.set("os", os).set("url", url).set("version_no", versionNo).set("status", status)
			.set("is_force", isForce).set("create_time", new Date()).save();
		}
		return InvokeResult.success();
	} 
	public AppVersion getNewsAppVersion(String os, String versionNo) {
		// TODO Auto-generated method stub
		AppVersion appVersion=this.getLastAppVersion(os, "miyue");
		if(appVersion!=null&&!appVersion.getStr("version_no").equals(versionNo)){
			return appVersion;
		}
		return null;
	}

	public AppVersion getLastAppVersion(String os, String appname) {
		// TODO Auto-generated method stub 
		Set<Condition> conditions=new HashSet<Condition>();
		conditions.add(new Condition("os",Operators.EQ,os));
		conditions.add(new Condition("title",Operators.EQ,appname));
		conditions.add(new Condition("visible",Operators.EQ,1));
		LinkedHashMap<String, String> orderby=new LinkedHashMap<String, String>();
		orderby.put("nature_no", "desc");
		List<AppVersion> list=dao.getList(1, 1, conditions, orderby);
		if(list.size()>0)return list.get(0);
		return null;
	}

	public Page<AppVersion> getPage(int page, int rows, LinkedHashMap<String, String> orderby) {
		return dao.getPage(page, rows,orderby);
	}
}
