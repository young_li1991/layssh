package com.generate.tpp;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.LogKit;

public class _TppTest {

    public static void main(String[] args) {
        String className = "goods";
        String tableName = "x_goods";

        JfGenerator.me
        .setSrcFolder("src/main/java")
        .setViewFolder("src/main/webapp/WEB-INF/generate")
        .setPackageBase("com.basemanage")
        .setBasePath("controller")
        .tableSql(getSqlList())
        .javaRender(className, tableName)
        .htmlRender(className, tableName);

        LogKit.info("---------OK-刷新一下项目吧---------");
    }
    
    /**
     * 建表语句
     * @return
     */
    private static List<String> getSqlList() {
        ArrayList<String> sqlList = new ArrayList<String>();
        
        sqlList.add("DROP TABLE IF EXISTS `t_test`;");
        sqlList.add("CREATE TABLE `t_test` ("+
		 " `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '编号',"+
		 " `remark` VARCHAR(255) DEFAULT NULL COMMENT '备注',"+
		 " `create_time` DATETIME DEFAULT NULL COMMENT '创建时间',"+
		 " `update_time` DATETIME DEFAULT NULL COMMENT '更新时间',"+
		 " `status` INT(11) NOT NULL DEFAULT '1' COMMENT '状态',"+
		 " PRIMARY KEY (`id`)"+
         ") ENGINE=INNODB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8");
        
        return sqlList;
    }
 
}
